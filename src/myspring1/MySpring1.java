/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myspring1;

import com.myspring.interfaces.ReportGenerator;
import com.myspring.services.ReportService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author gianburga
 */
public class MySpring1 {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        
        /*ReportGenerator report = (ReportGenerator) context.getBean("reportPDF");
        report.generate();*/
        
        ReportService reportService1 = (ReportService) context.getBean("reportService1");
        reportService1.calculateService();
        
        ReportService reportService2 = (ReportService) context.getBean("reportService2");
        reportService2.calculateService();

    }
    
}
