/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myspring.impl;

import com.myspring.interfaces.ReportGenerator;
import com.myspring.models.Report;
import java.util.List;

/**
 *
 * @author gianburga
 */
public class PDFReportGenerator implements ReportGenerator {
    
    private List<Report> reports;
    private int anno;

    public PDFReportGenerator(int anno){
        this.anno = anno;
    }

    @Override
    public void generate() {
        System.out.println("Report PDF " + getAnno() + " ...");
        for (Report report: getReports()) {
            System.out.println(report.getDate() + ": " + report.getBody());
        }
    }

    /**
     * @return the reports
     */
    public List<Report> getReports() {
        return reports;
    }

    /**
     * @param reports the reports to set
     */
    public void setReports(List<Report> reports) {
        this.reports = reports;
    }

    /**
     * @return the anno
     */
    public int getAnno() {
        return anno;
    }

    /**
     * @param anno the anno to set
     */
    public void setAnno(int anno) {
        this.anno = anno;
    }
    
}
