/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myspring.services;

import com.myspring.interfaces.ReportGenerator;

/**
 *
 * @author gianburga
 */
public class ReportService {
    private ReportGenerator reportGenerator;
    
    public void calculateService(){
        reportGenerator.generate();
    }

    /**
     * @return the reportGenerator
     */
    public ReportGenerator getReportGenerator() {
        return reportGenerator;
    }

    /**
     * @param reportGenerator the reportGenerator to set
     */
    public void setReportGenerator(ReportGenerator reportGenerator) {
        this.reportGenerator = reportGenerator;
    }
}
